// Qt includes
#include <QCoreApplication>

// QtWebServer includes
#include "tcp/tcpmultithreadedserver.h"
#include "http/httpwebengine.h"

// Own includes
#include "websocketserver.h"
#include "indexresource.h"

int main(int argc, char *argv[]) {
    QCoreApplication application(argc, argv);

    // Configure websocket server
    WebSocketServer webSocketServer;
    webSocketServer.listen(QHostAddress::Any, 8080);

    // Configure webserver
    QtWebServer::Http::WebEngine webEngine;
    //webEngine.addResource(new IndexResource());

    QtWebServer::Tcp::MultithreadedServer multithreadedWebServer;
    multithreadedWebServer.setResponder(&webEngine);
    multithreadedWebServer.listen(QHostAddress::Any, 80);

    // Run application
    return application.exec();
}
