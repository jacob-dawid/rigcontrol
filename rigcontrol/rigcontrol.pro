QT += core
QT -= gui

TARGET = rigcontrol
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    systemstatus.cpp \
    statusresource.cpp \
    controlresource.cpp \
    rigcontrol.cpp \
    ethminerresource.cpp

HEADERS += \
    systemstatus.h \
    statusresource.h \
    controlresource.h \
    rigcontrol.h \
    ethminerresource.h

include(../pods.pri)
