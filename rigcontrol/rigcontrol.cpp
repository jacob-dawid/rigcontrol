///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "rigcontrol.h"

#ifdef Q_OS_WIN
#include "qt_windows.h"
#endif
#ifdef Q_OS_LINUX
#include <sys/reboot.h>
#endif

RigControl::RigControl(int &argc, char **argv) :
    QCoreApplication(argc, argv),
    _settings(new QSettings("omg-it.works", "rigcontrol")) {

    _ethereumMiner = new EthereumMiner();
    _settings->beginGroup("ethereum_miner");
    _ethereumMiner->loadSettings(_settings);
    _settings->endGroup();

    _ethereumMiner->processInBackground();

    _systemStatus = new SystemStatus();
    _amdOverdrive = new AMDOverdrive();
}

bool RigControl::shutdown() {
#ifdef Q_OS_WIN
 return ExitWindowsEx(EWX_SHUTDOWN, 0);
#endif
#ifdef Q_OS_LINUX
 return ::reboot(RB_POWER_OFF) == 0;
#endif
}

bool RigControl::reboot() {
#ifdef Q_OS_WIN
 return ExitWindowsEx(EWX_REBOOT, 0);
#endif
#ifdef Q_OS_LINUX
 return ::reboot(RB_AUTOBOOT) == 0;
#endif
}

QSettings *RigControl::settings() {
    return _settings;
}

