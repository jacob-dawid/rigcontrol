///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "sys/sysinfo.h"

#include <QString>

class SystemStatus {
public:
    SystemStatus();

    QString operatingSystemString();
    QList<QString> ips();

    bool isOnline();

    int uptime();
    int freeRAM();
    int totalRAM();
    float cpuLoad();
    int numberOfProcesses();

private:
    struct sysinfo _sys_info;
};
