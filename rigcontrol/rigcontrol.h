///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <QCoreApplication>
#include <QSettings>

#include "systemstatus.h"
#include "amdoverdrive.h"
#include "ethereumminer.h"

class RigControl :
    public QCoreApplication {
    Q_OBJECT
public:
    RigControl(int &argc, char **argv);

    SystemStatus *systemStatus() { return _systemStatus; }
    AMDOverdrive *amdOverdrive() { return _amdOverdrive; }
    EthereumMiner *ethereumMiner() { return _ethereumMiner; }

    bool shutdown();
    bool reboot();

    QSettings *settings();

private:
    QSettings *_settings;

    SystemStatus *_systemStatus;
    AMDOverdrive *_amdOverdrive;
    EthereumMiner *_ethereumMiner;
};
