///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "systemstatus.h"

#include <QSysInfo>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkConfigurationManager>
#include <QNetworkReply>
#include <QNetworkInterface>
#include <QTimer>
#include <QObject>
#include <QThread>

SystemStatus::SystemStatus() {
    sysinfo(&_sys_info);
}

QString SystemStatus::operatingSystemString() {
    return QSysInfo::prettyProductName();
}

QList<QString> SystemStatus::ips() {
    QList<QString> ipList;
    QList<QHostAddress> addresses = QNetworkInterface::allAddresses();
    foreach(QHostAddress ha, addresses) {
        if(ha.protocol() == QAbstractSocket::IPv4Protocol && ha != QHostAddress(QHostAddress::LocalHost))
            ipList << ha.toString();
    }

    return ipList;
}

bool SystemStatus::isOnline() {
    // This only checks for an active network connection.
    QNetworkConfigurationManager ncm;
    if(!ncm.isOnline()) {
        return false;
    }

    QNetworkAccessManager nam;
    QTimer timeoutTimer;
    timeoutTimer.setSingleShot(true);
    timeoutTimer.setInterval(300);
    QNetworkReply *reply = nam.get(QNetworkRequest(QUrl("http://www.msftncsi.com/ncsi.txt")));
    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    QObject::connect(&timeoutTimer, SIGNAL(timeout()), &loop, SLOT(quit()));
    loop.exec();
    timeoutTimer.start();

    bool online = reply->isFinished() && (reply->error() == QNetworkReply::NoError);
    reply->deleteLater();

    return online;
}

int SystemStatus::uptime() {
    return _sys_info.uptime;
}

int SystemStatus::freeRAM() {
    return (_sys_info.freeram / 1048576);
}

int SystemStatus::totalRAM() {
    return (_sys_info.totalram / 1048576);
}

float SystemStatus::cpuLoad() {
    return (_sys_info.loads[0] * 100.0 / (float)(1 << SI_LOAD_SHIFT)) / (double)QThread::idealThreadCount();
}

int SystemStatus::numberOfProcesses() {
    return (_sys_info.procs);
}
