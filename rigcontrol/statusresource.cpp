///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "statusresource.h"

#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>

StatusResource::StatusResource(RigControl *rigControl) :
    QtWebServer::Http::Resource("/api/status") {
    _rigControl = rigControl;
}

StatusResource::~StatusResource() {
}

void StatusResource::deliver(const QtWebServer::Http::Request& request,
                            QtWebServer::Http::Response& response) {
    response.setStatusCode(QtWebServer::Http::Ok);
    response.setHeader(QtWebServer::Http::ContentType, "text/plain");

    QJsonDocument responseDocument;
    QJsonObject documentObject;

    SystemStatus *systemStatus = _rigControl->systemStatus();
    QJsonObject systemStatusObject;
    systemStatusObject.insert("operating_system", systemStatus->operatingSystemString());
    systemStatusObject.insert("cpu_load", systemStatus->cpuLoad());
    systemStatusObject.insert("number_of_processes", systemStatus->numberOfProcesses());
    systemStatusObject.insert("free_ram_mb", systemStatus->freeRAM());
    systemStatusObject.insert("total_ram_mb", systemStatus->totalRAM());
    systemStatusObject.insert("uptime_secs", systemStatus->uptime());
    systemStatusObject.insert("is_online", systemStatus->isOnline());

    QString ipString;
    QList<QString> ipList = systemStatus->ips();
    foreach(QString ip, ipList) {
        if(!ipString.isEmpty()) {
            ipString.append(", ");
        }
        ipString.append(ip);
    }
    systemStatusObject.insert("ips", ipString);

    documentObject.insert("system_status", systemStatusObject);

    AMDOverdrive *amdOverdrive = _rigControl->amdOverdrive();
    QJsonObject gpuStatusObject;

    QList<AdapterInfo> adaptersInfo = amdOverdrive->adaptersInfo();
    QStringList knownAdapterUDIDs;

    foreach(AdapterInfo ai, adaptersInfo) {
        QJsonObject graphicsCardObject;
        ADLPMActivity activity = amdOverdrive->currentActivity(ai.iAdapterIndex);
        ADLPowerControlInfo powerControlInfo = amdOverdrive->powerControlInfo(ai.iAdapterIndex);
        ADLBiosInfo biosInfo = amdOverdrive->biosInfo(ai.iAdapterIndex);
        QList<AMDOverdrive::PerformanceLevelInfo> pliList = amdOverdrive->performanceLevels(ai.iAdapterIndex);
        QList<ADLThermalControllerInfo> thermalControllersInfo = amdOverdrive->thermalControllersInfo(ai.iAdapterIndex);
        AMDOverdrive::Capabilities caps = amdOverdrive->capabilities(ai.iAdapterIndex);

        QString udid = ai.strUDID;
        if(knownAdapterUDIDs.contains(udid)) {
            continue;
        } else {
            knownAdapterUDIDs.append(udid);
        }

        graphicsCardObject.insert("id", amdOverdrive->adapterID(ai.iAdapterIndex));

        graphicsCardObject.insert("bios_date", biosInfo.strDate);
        graphicsCardObject.insert("bios_part_number", biosInfo.strPartNumber);
        graphicsCardObject.insert("bios_version", biosInfo.strVersion);

        graphicsCardObject.insert("od_enabled", caps.enabled);
        graphicsCardObject.insert("od_supported", caps.supported);
        graphicsCardObject.insert("od_version", caps.version);

        graphicsCardObject.insert("adapter_index", ai.iAdapterIndex);
        graphicsCardObject.insert("bus_number", ai.iBusNumber);
        graphicsCardObject.insert("device_number", ai.iDeviceNumber);
        graphicsCardObject.insert("drv_index", ai.iDrvIndex);
        graphicsCardObject.insert("function_number", ai.iFunctionNumber);
        graphicsCardObject.insert("present", ai.iPresent);
        graphicsCardObject.insert("vendor_id", ai.iVendorID);
        graphicsCardObject.insert("x_screen_num", ai.iXScreenNum);
        graphicsCardObject.insert("adapter_name", ai.strAdapterName);
        graphicsCardObject.insert("display_name", ai.strDisplayName);
        graphicsCardObject.insert("UDID", udid);
        graphicsCardObject.insert("x_screen_config_name", ai.strXScreenConfigName);

        graphicsCardObject.insert("core_clock_mhz", activity.iEngineClock / 100);
        graphicsCardObject.insert("memory_clock_mhz", activity.iMemoryClock / 100);
        graphicsCardObject.insert("core_voltage_vdc", activity.iVddc);
        graphicsCardObject.insert("performance_level", activity.iCurrentPerformanceLevel);
        graphicsCardObject.insert("load", activity.iActivityPercent);
        graphicsCardObject.insert("bus_lanes", activity.iCurrentBusLanes);
        graphicsCardObject.insert("maximum_bus_lanes", activity.iMaximumBusLanes);
        graphicsCardObject.insert("bus_speed", activity.iCurrentBusSpeed);

        graphicsCardObject.insert("power_control_supported", amdOverdrive->isPowerControlSupported(ai.iAdapterIndex));
        graphicsCardObject.insert("power_control_current", amdOverdrive->powerControlGetCurrent(ai.iAdapterIndex));
        graphicsCardObject.insert("power_control_default", amdOverdrive->powerControlGetDefault(ai.iAdapterIndex));
        graphicsCardObject.insert("power_control_min", powerControlInfo.iMinValue);
        graphicsCardObject.insert("power_control_step", powerControlInfo.iStepValue);
        graphicsCardObject.insert("power_control_max", powerControlInfo.iMaxValue);

        QJsonObject performanceLevelsObject;
        int performanceLevel = 0;
        foreach(AMDOverdrive::PerformanceLevelInfo pli, pliList) {
            QJsonObject performanceLevelObject;
            performanceLevelObject.insert("default_core_clock_mhz", pli.stock.iEngineClock / 100);
            performanceLevelObject.insert("default_memory_clock_mhz", pli.stock.iMemoryClock / 100);
            performanceLevelObject.insert("default_core_voltage_vdc", pli.stock.iVddc);
            performanceLevelObject.insert("current_core_clock_mhz", pli.current.iEngineClock / 100);
            performanceLevelObject.insert("current_memory_clock_mhz", pli.current.iMemoryClock / 100);
            performanceLevelObject.insert("current_core_voltage_vdc", pli.current.iVddc);
            performanceLevelsObject.insert(QString("level_%1").arg(performanceLevel), performanceLevelObject);
            performanceLevel++;
        }
        graphicsCardObject.insert("performance_levels", performanceLevelsObject);


        QJsonObject thermalControllersObject;
        int thermalControllerIndex = 0;
        foreach(ADLThermalControllerInfo tci, thermalControllersInfo) {
            ADLFanSpeedInfo fanSpeedInfo = amdOverdrive->fanSpeedInfo(ai.iAdapterIndex, tci.iDomainIndex);

            QJsonObject thermalControllerObject;
            thermalControllerObject.insert("temperature_mC", amdOverdrive->temperatureMillidegreesCelsius(ai.iAdapterIndex, tci.iDomainIndex));
            thermalControllerObject.insert("fan_supports_percent_read", amdOverdrive->fanSupportsPercentRead(fanSpeedInfo));
            thermalControllerObject.insert("fan_supports_percent_write", amdOverdrive->fanSupportsPercentWrite(fanSpeedInfo));
            thermalControllerObject.insert("fan_supports_rpm_read", amdOverdrive->fanSupportsRpmRead(fanSpeedInfo));
            thermalControllerObject.insert("fan_supports_rpm_write", amdOverdrive->fanSupportsRpmWrite(fanSpeedInfo));
            if(amdOverdrive->fanSupportsPercentRead(fanSpeedInfo)) {
                thermalControllerObject.insert("fan_value_percent", amdOverdrive->fanSpeedValue(ai.iAdapterIndex, tci.iThermalDomain, AMDOverdrive::Percent));
            }
            if(amdOverdrive->fanSupportsRpmRead(fanSpeedInfo)) {
                thermalControllerObject.insert("fan_value_rpm", amdOverdrive->fanSpeedValue(ai.iAdapterIndex, tci.iThermalDomain, AMDOverdrive::Rpm));
            }
            thermalControllersObject.insert(QString("controller_%1").arg(tci.iDomainIndex), thermalControllerObject);
            thermalControllerIndex++;
        }
        graphicsCardObject.insert("thermal_controllers", thermalControllersObject);

        gpuStatusObject.insert(QString("gpu_%1").arg(ai.iAdapterIndex), graphicsCardObject);
    }

    documentObject.insert("gpu_status", gpuStatusObject);


//    if(parameters["resource"] == "minerlog") {
//        QProcess *process = new QProcess;
//        process->start("tail -n 15 /home/miner/tmp/miner.log");
//        process->waitForFinished();
//        result = process->readAllStandardOutput();
//    }

    responseDocument.setObject(documentObject);
    response.setBody(responseDocument.toJson());
}


