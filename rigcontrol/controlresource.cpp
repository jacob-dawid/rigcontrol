///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "controlresource.h"

#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

ControlResource::ControlResource(RigControl *rigControl) :
    QtWebServer::Http::Resource("/api/control") {
    _rigControl = rigControl;
}

ControlResource::~ControlResource() {
}

void ControlResource::deliver(const QtWebServer::Http::Request& request,
                            QtWebServer::Http::Response& response) {
    response.setStatusCode(QtWebServer::Http::Ok);
    response.setHeader(QtWebServer::Http::ContentType, "text/plain");

    QMap<QString, QByteArray> urlParameters = request.urlParameters();

    QJsonDocument responseDocument;
    QJsonObject documentObject;

    QString action = urlParameters.value("action");

    if(action == "shutdown") {
        documentObject.insert("shutdown", _rigControl->shutdown());
    }

    if(action == "reboot") {
        documentObject.insert("reboot", _rigControl->reboot());
    }

    responseDocument.setObject(documentObject);
    response.setBody(responseDocument.toJson());
}


