///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "ethminerresource.h"

#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

#ifdef Q_OS_WIN
#include "qt_windows.h"
#endif
#ifdef Q_OS_LINUX
#include <sys/reboot.h>
#endif

EthMinerResource::EthMinerResource(RigControl *rigControl) :
    QtWebServer::Http::Resource("/api/ethminer") {
    _rigControl = rigControl;
}

EthMinerResource::~EthMinerResource() {
}

void EthMinerResource::deliver(
    const QtWebServer::Http::Request& request,
    QtWebServer::Http::Response& response) {
    response.setStatusCode(QtWebServer::Http::Ok);
    response.setHeader(QtWebServer::Http::ContentType, "text/plain");

//    QMap<QString, QByteArray> urlParameters = request.urlParameters();

    QJsonDocument responseDocument;
    QJsonObject documentObject;

    if(request.method() == "get") {
        EthereumMiner *em = _rigControl->ethereumMiner();
        EthereumMiner::Step currentStep = em->currentStep();
        EthereumMiner::Configuration config = em->configuration();

        switch(currentStep) {
            case EthereumMiner::Starting:
                documentObject.insert("step", "starting");
                break;
            case EthereumMiner::Halted:
                documentObject.insert("step", "halted");
                break;
            case EthereumMiner::Connecting:
                documentObject.insert("step", "connecting");
                break;
            case EthereumMiner::LoggingIn:
                documentObject.insert("step", "logging_in");
                break;
            case EthereumMiner::Mining:
                documentObject.insert("step", "mining");
                break;
            case EthereumMiner::BuildingDAG:
                documentObject.insert("step", "building_dag");
                break;
        }

        QJsonObject configurationObject;
        switch(config.minerType) {
          case EthereumMiner::CPUMiner:
            configurationObject.insert("miner_type", "cpu");
            break;
          case EthereumMiner::OpenCLMiner:
            configurationObject.insert("miner_type", "opencl");
            break;
        }

        configurationObject.insert("max_mining_threads", (int)config.maxMiningThreads);
        configurationObject.insert("current_block", (int)config.currentBlock);
        configurationObject.insert("precompute_next_dag", config.precomputeNextDAG);

        QJsonObject stratumObject;
        stratumObject.insert("server", config.server);
        stratumObject.insert("port", (int)config.port);
        stratumObject.insert("username", config.username);
        stratumObject.insert("password", config.password);
        configurationObject.insert("stratum", stratumObject);

        QJsonObject openclObject;
        openclObject.insert("platform", (int)config.openclPlatform);
        openclObject.insert("device", (int)config.openclDevice);
        openclObject.insert("global_work", (int)config.globalWorkSizeMultiplier);
        openclObject.insert("local_work", (int)config.localWorkSize);
        openclObject.insert("ms_per_batch", (int)config.msPerBatch);
        openclObject.insert("extra_gpu_memory", (int)config.extraGPUMemory);
        openclObject.insert("cpu_as_opencl_device", config.recognizeCPUAsOpenCLDevice);
        configurationObject.insert("opencl", openclObject);

        documentObject.insert("configuration", configurationObject);
    }

    if(request.method() == "post") {
        QJsonObject jsonRequestObject = QJsonDocument::fromJson(request.body()).object();
        QString method = jsonRequestObject.value("method").toString();

        if(method == "startMining") {
            _rigControl->ethereumMiner()->startMining();
            documentObject.insert("result", true);
        } else
        if(method == "stopMining") {
            _rigControl->ethereumMiner()->stopMining();
            documentObject.insert("result", true);
        } else
        if(method == "restartMining") {
            _rigControl->ethereumMiner()->restartMining();
            documentObject.insert("result", true);
        } else {
            documentObject.insert("error", "The requested method is not valid.");
            documentObject.insert("result", false);
        }
    }

    responseDocument.setObject(documentObject);
    response.setBody(responseDocument.toJson());
}
