///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControl.                                       //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControl is free software: you can redistribute it and/or modify     //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControl is distributed in the hope that it will be useful,          //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControl. If not, see <http://www.gnu.org/licenses/>.     //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

// Qt includes
#include <QCoreApplication>
#include <QDebug>
#include <QThread>

// QtWebServer includes
#include "tcp/tcpmultithreadedserver.h"
#include "http/httpwebengine.h"
#include "util/utilassetsresource.h"

#include "statusresource.h"
#include "controlresource.h"
#include "ethminerresource.h"

#include "rigcontrol.h"

int main(int argc, char *argv[]) {
    RigControl rigControl(argc, argv);

    QtWebServer::Http::WebEngine webEngine;
    webEngine.addResource(new StatusResource(&rigControl));
    webEngine.addResource(new ControlResource(&rigControl));
    webEngine.addResource(new EthMinerResource(&rigControl));

    QtWebServer::Tcp::MultithreadedServer multithreadedWebServer;
    multithreadedWebServer.setResponder(&webEngine);
    if(multithreadedWebServer.listen(QHostAddress::Any, 8080)) {
        qDebug() << "Started API Server.";
    } else {
        qDebug() << "Failed to start API Server. Check permissions and port.";
        return 1;
    }

    return rigControl.exec();
}
