// QtWebServer includes
#include "util/utilformurlcodec.h"

// Own includes
#include "systemcontrolwebwidget.h"
#include "amddisplaylibrary.h"

#include <QFile>
#include <QTextStream>
#include <QProcess>

SystemControlWebWidget::SystemControlWebWidget() {
}

QString SystemControlWebWidget::renderHtml(const QtWebServer::Http::Request &request) {
    if(request.method() == "post") {
        QMap<QString, QByteArray> formParameters
            = QtWebServer::Util::FormUrlCodec::decodeFormUrl(request.body());

        if(formParameters["action"] == "reboot") {
            system("echo \"miner\" | sudo -S reboot");
        }

        if(formParameters["action"] == "shutdown") {
            system("echo \"miner\" | sudo -S shutdown -h 0");
        }

        if(formParameters["action"] == "stop-mining") {
            system("/home/miner/stop-miner");
        }

        if(formParameters["action"] == "restart-mining") {
            system("/home/miner/restart-miner");
        }

        if(formParameters["action"] == "preserve-gpu-config") {
            QFile file("/home/miner/set-clocks");
            if(file.open(QIODevice::WriteOnly)) {
               QTextStream stream(&file);
               stream << "#!/bin/bash\n";
               AMDDisplayLibrary adl;
               adl.update();
               for(int i=0; i < adl.numberOfCards(); i++) {
                   stream << "/home/miner/adl3/atitweak --adapter=" << i
                   << " --set-engine-clock=" << adl.coreClock(i)
                   << " --set-memory-clock=" << adl.memoryClock(i)
                   << " --set-powertune=" << adl.powertuneValue(i)
                   << " --set-fan-speed=" << adl.fanSpeed(i)
                   << "\n";
               }
               file.close();
            }
        }

        if(formParameters["action"] == "restore-gpu-config") {
            system("/home/miner/set-clocks");
        }

        if(formParameters["action"] == "delete-gpu-config") {
            system("echo '#!/bin/bash\n' > /home/miner/set-clocks");
        }

        if(formParameters["action"] == "save") {
            QFile file1("/home/miner/configuration/miner_name");
                   if(file1.open(QIODevice::WriteOnly)) {
               QTextStream stream(&file1);
               stream << formParameters["minername"];
               file1.close();
          }

            QFile file2("/home/miner/configuration/payout_address");
                   if(file2.open(QIODevice::WriteOnly)) {
               QTextStream stream(&file2);
               stream << formParameters["payoutaddress"];
               file2.close();
          }

        }

    }

        QProcess *process = new QProcess;
        process->start("cat /home/miner/configuration/miner_name");
        process->waitForFinished();
        QString minerName = process->readAllStandardOutput();

        process = new QProcess;
        process->start("cat /home/miner/configuration/payout_address");
        process->waitForFinished();
        QString payoutAddress = process->readAllStandardOutput();

    return
        "<center>"
            "<form action=\"/\" method=\"post\">"
"Miner name:"
  "<input type='text' name='minername' value='" + minerName + "'>|</input>"
"Payout address:"
  "<input type='text' name='payoutaddress' value='" + payoutAddress + "'>|</input>"
               "<button class=\"btn btn-primary\" type=\"submit\" name=\"action\" value=\"save\">"
                    "Save"
                "</button>"


            "<div class=\"btn-group\">"
                "<button class=\"btn btn-warning\" type=\"submit\" name=\"action\" value=\"reboot\">"
                    "Reboot mining rig"
                "</button>"
                "<button class=\"btn btn-warning\" style='margin-right: 20px;' type=\"submit\" name=\"action\" value=\"shutdown\">"
                    "Shutdown mining rig"
                "</button>"
                "<button class=\"btn btn-default\" type=\"submit\" name=\"action\" value=\"stop-mining\">"
                    "Stop mining"
                "</button>"
                "<button class=\"btn btn-primary\" style='margin-right: 20px;' type=\"submit\" name=\"action\" value=\"restart-mining\">"
                    "(Re-)start mining"
                "</button>"

                "<button class=\"btn btn-danger\" type=\"submit\" name=\"action\" value=\"preserve-gpu-config\">"
                    "Preserve GPU config"
                "</button>"
                "<button class=\"btn btn-danger\" type=\"submit\" name=\"action\" value=\"delete-gpu-config\">"
                    "Delete GPU config"
                "</button>"
                "<button class=\"btn btn-danger\" type=\"submit\" name=\"action\" value=\"restore-gpu-config\">"
                    "Restore GPU config"
                "</button>"

            "</div>"
            "</form>"
        "</center>";
}
