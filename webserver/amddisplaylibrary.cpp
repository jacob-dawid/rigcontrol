// Qt includes
#include <QProcess>
#include <QTextStream>
#include <QDebug>

// Own includes
#include "amddisplaylibrary.h"

AMDDisplayLibrary::AMDDisplayLibrary() {
}

void AMDDisplayLibrary::update() {
    QProcess *p =new QProcess;
    p->start("/home/miner/adl3/atitweak -s");
    p->waitForFinished();
    _output = p->readAllStandardOutput();
    parseOutput();
}

int AMDDisplayLibrary::numberOfCards() {
    return _numberOfCards;
}

QString AMDDisplayLibrary::cardName(int index) {
    return _cardNames.at(index);
}

int AMDDisplayLibrary::coreClock(int index) {
    return _coreClocks.at(index);
}

int AMDDisplayLibrary::memoryClock(int index) {
    return _memoryClocks.at(index);
}

float AMDDisplayLibrary::coreVoltage(int index) {
    return _coreVoltages.at(index);
}

int AMDDisplayLibrary::performanceLevel(int index) {
    return _performanceLevels.at(index);
}

int AMDDisplayLibrary::gpuLoad(int index) {
    return _gpuLoads.at(index);
}

int AMDDisplayLibrary::fanSpeed(int index) {
    return _fanSpeeds.at(index);
}

float AMDDisplayLibrary::temperature(int index) {
    return _temperatures.at(index);
}

int AMDDisplayLibrary::powertuneValue(int index) {
    return _powertuneValues.at(index);
}

void AMDDisplayLibrary::setCoreClock(int index, int coreClock) {
    QProcess *p =new QProcess;
    p->start(QString("/home/miner/adl3/atitweak --adapter=%1 --set-engine-clock=%2").arg(index).arg(coreClock));
    p->waitForFinished();
}

void AMDDisplayLibrary::setMemoryClock(int index, int memoryClock) {
    QProcess *p =new QProcess;
    p->start(QString("/home/miner/adl3/atitweak --adapter=%1 --set-memory-clock=%2").arg(index).arg(memoryClock));
    p->waitForFinished();
}

void AMDDisplayLibrary::setFanSpeed(int index, int fanSpeed) {
    if(fanSpeed < 20) {
        fanSpeed = 20;
    }

    if(fanSpeed > 100) {
        fanSpeed = 100;
    }

    QProcess *p =new QProcess;
    p->start(QString("/home/miner/adl3/atitweak --adapter=%1 --set-fan-speed=%2").arg(index).arg(fanSpeed));
    p->waitForFinished();
}

void AMDDisplayLibrary::setFanSpeedAuto(int index) {
    QProcess *p =new QProcess;
    p->start(QString("/home/miner/adl3/atitweak --adapter=%1 --set-fan-speed-default").arg(index));
    p->waitForFinished();
}

void AMDDisplayLibrary::setPowertuneValue(int index, int powertuneValue) {
    QProcess *p =new QProcess;
    p->start(QString("/home/miner/adl3/atitweak --adapter=%1 --set-powertune=%2").arg(index).arg(powertuneValue));
    p->waitForFinished();
}

void AMDDisplayLibrary::parseOutput() {
//    0. AMD Radeon HD 7900 Series  (:0.0)
//        engine clock 1000MHz, memory clock 1250MHz, core voltage 1.169VDC, performance level 2, utilization 99%
//        fan speed 58% (2674 RPM) (default)
//        temperature 72 C
//        powertune 0%
//    1. AMD Radeon HD 7900 Series  ()
//        engine clock 1000MHz, memory clock 1250MHz, core voltage 1.188VDC, performance level 0, utilization 98%
//        fan speed 61% (2606 RPM) (default)
//        temperature 91 C
//        powertune 0%
//    2. AMD Radeon HD 7900 Series ()
//        engine clock 1000MHz, memory clock 1250MHz, core voltage 1.2VDC, performance level 3, utilization 99%
//        fan speed 55% (2558 RPM) (default)
//        temperature 73 C
//        powertune 0%
//    3. AMD Radeon HD 7900 Series ()
//        engine clock 1000MHz, memory clock 1250MHz, core voltage 1.2VDC, performance level 3, utilization 99%
//        fan speed 57% (2658 RPM) (default)
//        temperature 75 C
//        powertune 0%
//    4. AMD Radeon HD 7900 Series  ()
//        engine clock 1000MHz, memory clock 1250MHz, core voltage 1.169VDC, performance level 0, utilization 100%
//        fan speed 63% (2700 RPM) (user-defined)
//        temperature 70.875 C
//        powertune 0%

    QTextStream textStream(&_output);
    _numberOfCards = 0;
    _cardNames.clear();
    _coreClocks.clear();
    _memoryClocks.clear();
    _coreVoltages.clear();
    _performanceLevels.clear();
    _gpuLoads.clear();
    _fanSpeeds.clear();
    _temperatures.clear();
    _powertuneValues.clear();

    QString line;

    while(!(line = textStream.readLine()).isEmpty()) {
        _numberOfCards++;
        _cardNames << line.mid(2).remove(QRegExp("\\(.*\\)"));

        line = textStream.readLine();
        QRegExp rx = QRegExp("\\s*engine clock ([0-9]+)MHz, memory clock ([0-9]+)MHz, core voltage ([0-9]+\\.[0-9]+)VDC, performance level ([0-9]+), utilization ([0-9]+)%");
        rx.indexIn(line);
        QStringList capturedValues = rx.capturedTexts();
        _coreClocks << capturedValues.at(1).toInt();
        _memoryClocks << capturedValues.at(2).toInt();
        _coreVoltages << capturedValues.at(3).toFloat();
        _performanceLevels << capturedValues.at(4).toInt();
        _gpuLoads << capturedValues.at(5).toInt();

        line = textStream.readLine();
	rx = QRegExp("\\s*fan speed ([0-9]+)");
        rx.indexIn(line);
	capturedValues = rx.capturedTexts();
	_fanSpeeds << capturedValues.at(1).toInt();

        line = textStream.readLine();
        rx = QRegExp("\\s*temperature ([0-9\\.]+)");
        rx.indexIn(line);
        capturedValues = rx.capturedTexts();
        _temperatures << capturedValues.at(1).toFloat();

        line = textStream.readLine();
        rx = QRegExp("\\s*powertune ([\\-0-9]+)");
        rx.indexIn(line);
        capturedValues = rx.capturedTexts();
        _powertuneValues << capturedValues.at(1).toInt();

    }
}
