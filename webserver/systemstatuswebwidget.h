#pragma once

#include <QString>

#include "webwidget.h"

class SystemStatusWebWidget :
    public QtWebServer::WebWidget {

public:
    SystemStatusWebWidget();

    QString renderHtml(const QtWebServer::Http::Request& request);

};
