#pragma once

// Qt includes
#include <QString>
#include <QStringList>
#include <QList>

class AMDDisplayLibrary {
public:
    AMDDisplayLibrary();

    void update();

    int numberOfCards();
    QString cardName(int index);
    int coreClock(int index);
    int memoryClock(int index);
    float coreVoltage(int index);
    int performanceLevel(int index);
    int gpuLoad(int index);
    int fanSpeed(int index);
    float temperature(int index);
    int powertuneValue(int index);

    void setCoreClock(int index, int coreClock);
    void setMemoryClock(int index, int memoryClock);
    void setFanSpeed(int index, int fanSpeed);
    void setFanSpeedAuto(int index);
    void setPowertuneValue(int index, int powertuneValue);

private:
    void parseOutput();

    QString _output;

    // Parsed
    int _numberOfCards;
    QStringList _cardNames;
    QList<int> _coreClocks;
    QList<int> _memoryClocks;
    QList<float> _coreVoltages;
    QList<int> _performanceLevels;
    QList<int> _gpuLoads;
    QList<int> _fanSpeeds;
    QList<float> _temperatures;
    QList<int> _powertuneValues;
};
