#pragma once

#include "webwidget.h"

class SystemControlWebWidget :
    public QtWebServer::WebWidget {

public:
    SystemControlWebWidget();

    QString renderHtml(const QtWebServer::Http::Request& request);
};
