// QtWebServer includes
#include "util/utilformurlcodec.h"

// Own includes
#include "gpustatuswebwidget.h"
#include "amddisplaylibrary.h"

GpuStatusWebWidget::GpuStatusWebWidget() {
}

QString GpuStatusWebWidget::renderHtml(const QtWebServer::Http::Request &request) {
    AMDDisplayLibrary adl;
    adl.update();

    if(request.method() == "post") {
        QMap<QString, QByteArray> formParameters
            = QtWebServer::Util::FormUrlCodec::decodeFormUrl(request.body());

        QString indexString = formParameters["index"];
        int index = indexString.toInt();
        bool allCards = (indexString == "all");

        if(formParameters["action"] == "fanspeed-less") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setFanSpeed(i, adl.fanSpeed(i) - 5);
                }
            } else {
                adl.setFanSpeed(index, adl.fanSpeed(index) - 5);
            }
        }

        if(formParameters["action"] == "fanspeed-more") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setFanSpeed(i, adl.fanSpeed(i) + 5);
                }
            } else {
                adl.setFanSpeed(index, adl.fanSpeed(index) + 5);
            }
        }

        if(formParameters["action"] == "fanspeed-auto") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setFanSpeedAuto(i);
                }
            } else {
                adl.setFanSpeedAuto(index);
            }
        }

        if(formParameters["action"] == "fanspeed-full") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setFanSpeed(i, 100);
                }
            } else {
                adl.setFanSpeed(index, 100);
            }
        }


        if(formParameters["action"] == "coreclock-850") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, 850);
                }
            } else {
                adl.setCoreClock(index, 850);
            }
        }
        if(formParameters["action"] == "coreclock-925") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, 925);
                }
            } else {
                adl.setCoreClock(index, 925);
            }

        }
        if(formParameters["action"] == "coreclock-1000") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, 1000);
                }
            } else {
                adl.setCoreClock(index, 1000);
            }

        }
        if(formParameters["action"] == "coreclock-1100") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, 1100);
                }
            } else {
                adl.setCoreClock(index, 1100);
            }
        }

        if(formParameters["action"] == "coreclock-m5") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, adl.coreClock(i) - 5);
                }
            } else {
                adl.setCoreClock(index, adl.coreClock(index) - 5);
            }
        }
        if(formParameters["action"] == "coreclock-p5") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, adl.coreClock(i) + 5);
                }
            } else {
                adl.setCoreClock(index, adl.coreClock(index) + 5);
            }
        }
        if(formParameters["action"] == "coreclock-m10") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, adl.coreClock(i) - 10);
                }
            } else {
                adl.setCoreClock(index, adl.coreClock(index) - 10);
            }
        }
        if(formParameters["action"] == "coreclock-p10") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setCoreClock(i, adl.coreClock(i) + 10);
                }
            } else {
                adl.setCoreClock(index, adl.coreClock(index) + 10);
            }
        }


        if(formParameters["action"] == "memoryclock-1250") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, 1250);
                }
            } else {
                adl.setMemoryClock(index, 1250);
            }
        }
        if(formParameters["action"] == "memoryclock-1375") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, 1375);
                }
            } else {
                adl.setMemoryClock(index, 1375);
            }
        }
        if(formParameters["action"] == "memoryclock-1500") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, 1500);
                }
            } else {
                adl.setMemoryClock(index, 1500);
            }
        }
        if(formParameters["action"] == "memoryclock-m5") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, adl.memoryClock(i) - 5);
                }
            } else {
                adl.setMemoryClock(index, adl.memoryClock(index) - 5);
            }
        }
        if(formParameters["action"] == "memoryclock-p5") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, adl.memoryClock(i) + 5);
                }
            } else {
                adl.setMemoryClock(index, adl.memoryClock(index) + 5);
            }
        }
        if(formParameters["action"] == "memoryclock-m10") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, adl.memoryClock(i) - 10);
                }
            } else {
                adl.setMemoryClock(index, adl.memoryClock(index) - 10);
            }
        }
        if(formParameters["action"] == "memoryclock-p10") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setMemoryClock(i, adl.memoryClock(i) + 10);
                }
            } else {
                adl.setMemoryClock(index, adl.memoryClock(index) + 10);
            }
        }

        if(formParameters["action"] == "powertune-min") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setPowertuneValue(i, -20);
                }
            } else {
                adl.setPowertuneValue(index, -20);
            }
        }
        if(formParameters["action"] == "powertune-max") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setPowertuneValue(i, 20);
                }
            } else {
                adl.setPowertuneValue(index, 20);
            }
        }

        if(formParameters["action"] == "powertune-less") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setPowertuneValue(i, adl.powertuneValue(i) - 5);
                }
            } else {
                adl.setPowertuneValue(index, adl.powertuneValue(index) - 5);
            }
        }
        if(formParameters["action"] == "powertune-more") {
            if(allCards) {
                for(int i=0; i < adl.numberOfCards(); i++) {
                    adl.setPowertuneValue(i, adl.powertuneValue(i) + 5);
                }
            } else {
                adl.setPowertuneValue(index, adl.powertuneValue(index) + 5);
            }
        }

    }

    QString cardStatus = "<tr><th>Card Name</th><th><div>Core Clock</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-850&amp;index=all\">850MHz</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-925&amp;index=all\">925MHz</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-1000&amp;index=all\">1000MHz</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-1100&amp;index=all\">1100MHz</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-m10&amp;index=all\">- 10MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-m5&amp;index=all\">- 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-p5&amp;index=all\">+ 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-p10&amp;index=all\">+ 10MHz</div>"
                              "</div>"

             "</th><th><div>Memory Clock</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1250&amp;index=all\">1250MHz</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1375&amp;index=all\">1375MHz</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1500&amp;index=all\">1500MHz</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-m10&amp;index=all\">- 10MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-m5&amp;index=all\">- 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-p5&amp;index=all\">+ 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-p10&amp;index=all\">+ 10MHz</div>"
                              "</div>"

             "</th><th>Core Voltage</th><th>Performance Level</th><th>GPU Load</th><th><div>Fan Speed</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-auto&amp;index=all\">Auto</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-full&amp;index=all\">100%</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-less&amp;index=all\">- 5%</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-more&amp;index=all\">+ 5%</div>"
                              "</div>"

             "</th><th>Temperature</th><th><div style='min-width: 140px;'>Powertune Value</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-min&amp;index=all\">- 20%</div>"
                              "<div class=\"btn btn-info ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-max&amp;index=all\">+ 20%</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-less&amp;index=all\">- 5%</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-more&amp;index=all\">+ 5%</div>"
                              "</div>"

                              "</th></tr>";

    for(int i = 0; i < adl.numberOfCards(); i++) {
        cardStatus += QString("<tr>");
        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=cardname'>-</pre></td>").arg(i);
        cardStatus += QString("<td>"
                              "<pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=coreclock'>"
                              "- MHz"
                              "</pre>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-850&amp;index=%1\">850MHz</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-925&amp;index=%1\">925MHz</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-1000&amp;index=%1\">1000MHz</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-1100&amp;index=%1\">1100MHz</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-m10&amp;index=%1\">- 10MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-m5&amp;index=%1\">- 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-p5&amp;index=%1\">+ 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=coreclock-p10&amp;index=%1\">+ 10MHz</div>"
                              "</div>"
                              "</td>").arg(i);

        cardStatus += QString("<td>"
                              "<pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=memoryclock'>"
                              "- MHz"
                              "</pre>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1250&amp;index=%1\">1250MHz</div>"
			      "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1375&amp;index=%1\">1375MHz</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-1500&amp;index=%1\">1500MHz</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-m10&amp;index=%1\">- 10MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-m5&amp;index=%1\">- 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-p5&amp;index=%1\">+ 5MHz</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=memoryclock-p10&amp;index=%1\">+ 10MHz</div>"
                              "</div>"
                              "</td>").arg(i);


        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=corevoltage'>- VDC</pre></td>").arg(i);
        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=performancelevel'>-</pre></td>").arg(i);
        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=load'>- %</pre></td>").arg(i);
        cardStatus += QString("<td>"
                              "<pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=fanspeed'>"
                              "- %"
                              "</pre>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-auto&amp;index=%1\">Auto</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-full&amp;index=%1\">100%</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-less&amp;index=%1\">- 5%</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=fanspeed-more&amp;index=%1\">+ 5%</div>"
                              "</div>"
                              "</td>").arg(i);

        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=temperature'>- C</pre></td>").arg(i);
        cardStatus += QString("<td><pre class='panel panel-default update' data-api='/api/gpuinfo?index=%1&amp;value=powertune'>- %</pre>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-min&amp;index=%1\">- 20%</div>"
                              "<div class=\"btn btn-primary ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-max&amp;index=%1\">+ 20%</div>"
                              "</div>"
                              "<div class=\"btn-group\">"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-less&amp;index=%1\">- 5%</div>"
                              "<div class=\"btn btn-default ajax-button\" data-api=\"/\" data-formdata=\"action=powertune-more&amp;index=%1\">+ 5%</div>"
                              "</div>"
                              "</td>").arg(i);
        cardStatus += "</tr>";
    }

    return "<table class=\"table table-striped\">" + cardStatus + "</table>";
}
