#pragma once

#include "webwidget.h"

class MinerLogWebWidget :
    public QtWebServer::WebWidget {
public:
    MinerLogWebWidget();

    QString renderHtml(const QtWebServer::Http::Request& request);
};
