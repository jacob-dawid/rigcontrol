// Qt includes
#include <QProcess>

// Own includes
#include "minerlogwebwidget.h"

MinerLogWebWidget::MinerLogWebWidget() {
}

QString MinerLogWebWidget::renderHtml(const QtWebServer::Http::Request &request) {
    Q_UNUSED(request)
    return "<div><pre class='update' data-api='/api/minerlog'>loading..</pre></div>";
}
