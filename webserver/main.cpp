// Qt includes
#include <QCoreApplication>
#include <QDebug>

// QtWebServer includes
#include "tcp/tcpmultithreadedserver.h"
#include "http/httpwebengine.h"
#include "util/utilassetsresource.h"

#include "indexresource.h"
#include "apiresource.h"

int main(int argc, char *argv[]) {
    QCoreApplication application(argc, argv);

    // Configure webserver
    QtWebServer::Http::WebEngine webEngine;
    webEngine.addResource(new IndexResource());
    webEngine.addResource(new ApiResource());

    QtWebServer::Util::AssetsResource *assets = new QtWebServer::Util::AssetsResource();
    assets->insertAsset("bootstrap.js", ":/bootstrap.min.js");
    assets->insertAsset("jquery.js", ":/jquery-2.2.2.min.js");

    assets->insertAsset("bootstrap.css", ":/bootstrap.min.css");

    assets->insertAsset("ethereum-logo.png", ":/ether_logo_small.png");

    webEngine.addResource(assets);

    QtWebServer::Tcp::MultithreadedServer multithreadedWebServer;
    multithreadedWebServer.setResponder(&webEngine);
    if(multithreadedWebServer.listen(QHostAddress::Any, 8080)) {
        qDebug() << "Started web interface..";
    }

    // Run application
    return application.exec();
}
