#pragma once

#include <QTimerEvent>

// QtWebServer includes
#include "http/httpresource.h"
#include "amddisplaylibrary.h"

class ApiResource :
    public QtWebServer::Http::Resource {
public:
    ApiResource();
    ~ApiResource();

    void deliver(const QtWebServer::Http::Request& request,
                 QtWebServer::Http::Response& response);

protected:
    void timerEvent(QTimerEvent*);

private:
    AMDDisplayLibrary adl;
};

