#pragma once

#include "webwidget.h"

class GpuStatusWebWidget :
    public QtWebServer::WebWidget {
public:
    GpuStatusWebWidget();

    QString renderHtml(const QtWebServer::Http::Request& request);
};
