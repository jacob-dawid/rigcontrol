#pragma once

#include "sys/sysinfo.h"

class SystemStatus {
public:
    SystemStatus();

    int uptime();
    int freeRAM();
    int totalRAM();
    float cpuLoad();
    int numberOfProcesses();

private:
    struct sysinfo _sys_info;
};
