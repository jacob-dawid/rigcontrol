#pragma once

#include "weblayout.h"

class MainWebLayout :
    public QtWebServer::WebLayout {
public:
    MainWebLayout();

    virtual QString renderHtml(const QtWebServer::Http::Request& request);
};
