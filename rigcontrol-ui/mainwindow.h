///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControlUI.                                     //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControlUI is free software: you can redistribute it and/or modify   //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControlUI is distributed in the hope that it will be useful,        //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControlUI. If not, see <http://www.gnu.org/licenses/>.   //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void on_pushButtonAddRig_clicked();

    void on_treeWidgetRigs_itemSelectionChanged();

    void on_pushButtonSystemShutdown_clicked();
    void on_pushButtonSystemReboot_clicked();

protected:
    void timerEvent(QTimerEvent *);

private:
    void updateRigTabWidgetFromJson(QJsonDocument jsonDocument);

    Ui::MainWindow *ui;
};
