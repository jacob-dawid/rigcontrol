///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//    This file is part of RigControlUI.                                     //
//    Copyright (C) 2015-2016 Jacob Dawid, jacob@omg-it.works                //
//                                                                           //
//    RigControlUI is free software: you can redistribute it and/or modify   //
//    it under the terms of the GNU Affero General Public License as         //
//    published by the Free Software Foundation, either version 3 of the     //
//    License, or (at your option) any later version.                        //
//                                                                           //
//    RigControlUI is distributed in the hope that it will be useful,        //
//    but WITHOUT ANY WARRANTY; without even the implied warranty of         //
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          //
//    GNU Affero General Public License for more details.                    //
//                                                                           //
//    You should have received a copy of the GNU General Public License      //
//    along with RigControlUI. If not, see <http://www.gnu.org/licenses/>.   //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTreeWidgetItem>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>
#include <QTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    startTimer(1000);
    ui->tabWidgetRig->setEnabled(false);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_pushButtonAddRig_clicked() {
    QTreeWidgetItem *item = new QTreeWidgetItem();

    item->setIcon(0, QIcon("://icons/server-rack.svg"));
    item->setData(0, Qt::DisplayRole, ui->lineEditAddRigName->text());
    item->setData(1, Qt::DisplayRole, ui->lineEditAddRigAddress->text());
    item->setData(2, Qt::DisplayRole, "Attempting to connect..");
    item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    ui->treeWidgetRigs->insertTopLevelItem(ui->treeWidgetRigs->topLevelItemCount(), item);
}

void MainWindow::on_treeWidgetRigs_itemSelectionChanged() {
    ui->tabWidgetRig->setEnabled(false);
}

void MainWindow::on_pushButtonSystemShutdown_clicked() {
    QTreeWidgetItem* item = ui->treeWidgetRigs->selectedItems().first();
    QString address = item->data(1, Qt::DisplayRole).toString();

    QNetworkAccessManager nam;
    QNetworkReply *reply = nam.get(QNetworkRequest(QUrl(address + "/api/control?action=shutdown")));
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
}

void MainWindow::on_pushButtonSystemReboot_clicked() {
    QTreeWidgetItem* item = ui->treeWidgetRigs->selectedItems().first();
    QString address = item->data(1, Qt::DisplayRole).toString();

    QNetworkAccessManager nam;
    QNetworkReply *reply = nam.get(QNetworkRequest(QUrl(address + "/api/control?action=reboot")));
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
}

void MainWindow::timerEvent(QTimerEvent *) {
    int count = ui->treeWidgetRigs->topLevelItemCount();

    for(int i = 0; i < count; i++) {
        QTreeWidgetItem *item = ui->treeWidgetRigs->topLevelItem(i);
        QString address = item->data(1, Qt::DisplayRole).toString();

        QNetworkAccessManager nam;

        QTime pingTimer;
        pingTimer.start();
        QNetworkReply *reply = nam.get(QNetworkRequest(QUrl(address + "/api/status")));
        QEventLoop loop;
        connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        loop.exec();
        int ping = pingTimer.elapsed();

        if(reply->error() == QNetworkReply::NoError) {

            item->setData(2, Qt::DisplayRole, QString("Connected (%1)").arg(ping));
            item->setIcon(2, QIcon("://icons/1468065633_Circle_Green.png"));
            if(item->isSelected()) {
                updateRigTabWidgetFromJson(QJsonDocument::fromJson(reply->readAll()));
            }
        } else {
            item->setData(2, Qt::DisplayRole, reply->errorString());
            item->setIcon(2, QIcon("://icons/1468065586_Circle_Red.png"));
            if(item->isSelected()) {
                ui->tabWidgetRig->setEnabled(false);
            }
        }
    }
}

void MainWindow::updateRigTabWidgetFromJson(QJsonDocument jsonDocument) {
    QJsonObject systemStatus = jsonDocument.object().value("system_status").toObject();

    QString operatingSystem = systemStatus.value("operating_system").toString();
    QString cpuLoad = QString("%1 %").arg(systemStatus.value("cpu_load").toDouble());
    QString freeRamMb = QString("%1 MB").arg(systemStatus.value("free_ram_mb").toInt());
    QString numberOfProcesses = QString("%1").arg(systemStatus.value("number_of_processes").toInt());
    QString totalRamMb = QString("%1 MB").arg(systemStatus.value("total_ram_mb").toInt());

    QTime time = QTime(0, 0, 0).addSecs(systemStatus.value("uptime_secs").toInt());
    QString uptimeSecs = time.toString(Qt::ISODate);

    QString isOnline = systemStatus.value("is_online").toBool() ? "Yes" : "No";
    QString ips = systemStatus.value("ips").toString();

    QIcon operatingSystemIcon;
    if(operatingSystem.toLower().contains("win")) {
        operatingSystemIcon = QIcon("://icons/windows-symbol.svg");
    } else
    if(operatingSystem.toLower().contains("ubuntu")) {
        operatingSystemIcon = QIcon("://icons/ubuntu-logo.svg");
    } else {
        operatingSystemIcon = QIcon("://icons/linux.svg");
    }

    ui->tableWidgetSystemStatus->setItem(0, 0, new QTableWidgetItem(operatingSystemIcon, operatingSystem));
    ui->tableWidgetSystemStatus->setItem(0, 1, new QTableWidgetItem(cpuLoad));
    ui->tableWidgetSystemStatus->setItem(0, 2, new QTableWidgetItem(freeRamMb));
    ui->tableWidgetSystemStatus->setItem(0, 3, new QTableWidgetItem(totalRamMb));
    ui->tableWidgetSystemStatus->setItem(0, 4, new QTableWidgetItem(numberOfProcesses));
    ui->tableWidgetSystemStatus->setItem(0, 5, new QTableWidgetItem(uptimeSecs));
    ui->tableWidgetSystemStatus->setItem(0, 6, new QTableWidgetItem(isOnline));
    ui->tableWidgetSystemStatus->setItem(0, 7, new QTableWidgetItem(ips));

    ui->tabWidgetRig->setEnabled(true);
}

